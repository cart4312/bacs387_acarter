﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MessageCleaner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /*
         * MS: Find better descriptive names than "Btn" and "Button" 
         * Take advantage of the English language portion of programming
         * to make your programs more readable
         */
        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            Message msg = new Message();
            msg.OriginalText = InputBox.Text;
            Eraser erase = new Eraser();
            erase.BadText = WordBox.Text;

            Message cleanmsg = erase.Clean(msg);
            OutputBox.Text = cleanmsg.CleanText;
            
        }
    }
}
