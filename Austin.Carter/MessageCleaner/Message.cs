﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageCleaner
{
    class Message
    {
        public string OriginalText { get; set; }
        public string CleanText { get; set; }
    }
}
