﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageCleaner
{
    class Eraser
    {
        public string BadText { get; set; }
        public Message Clean(Message theMessage)
        {
            if(theMessage.OriginalText.Contains(BadText))
            {
                theMessage.CleanText = theMessage.OriginalText.Replace(BadText, "");
            }

            return theMessage;
        }


        
    }
}
