﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Message
    {
        public string Text { get; set; }
        public int CharacterLimit { get; set; }
        public DateTime SentAt { get; set; }
        public User SendAs { get; set; }

        public virtual void Send()
        {
            //lookup endpoint
            //compose message
            //transmit
            //confirm receipt from server
        }
        public void Delete()
        {

        }
    }
}
