﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Tweet : Message
    {
        public Tweet()
        {
            CharacterLimit = 140;
        }

        public int Likes { get; set; }
    }
}
