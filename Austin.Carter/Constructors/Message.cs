﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructors
{
    class Message
    {
        public Message()
        {
            
        }

        public Message(int characterLimit)
        {
            CharacterLimit = characterLimit;
        }

        public string Text { get; set; }
        public int CharacterLimit { get; set; }
    }
}
