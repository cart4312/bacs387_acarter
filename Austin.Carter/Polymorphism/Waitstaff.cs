﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Waitstaff : Employee
    {
        public decimal Tips { get; set; }
        public override decimal CalculatePay()
        {
            decimal totalPay = ((decimal)Hours * PayRate) + Tips;
            return totalPay;
        }
    }
}
