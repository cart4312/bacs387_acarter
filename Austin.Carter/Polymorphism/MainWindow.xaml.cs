﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Polymorphism
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double hoursvalue = Convert.ToDouble(hours.Text);
            decimal payvalue = Convert.ToDecimal(pay.Text);

            Employee emp = new Employee();
            manager.Hours = hoursvalue;
            manager.PayRate = payvalue;

            Manager manager = new Manager();
            manager.Hours = hoursvalue;
            manager.PayRate = payvalue;
            manager.Bonus = 100m;

            Waitstaff wait = new Waitstaff();
            wait.Hours = hoursvalue;
            wait.PayRate = payvalue;
            wait.Tips = 700;

            // decimal empPlay = emp.CalculatePay();
            // decimal managerPay = manager.Calculatepay();
            // decimal waitPay = wait.CalculatePay();

            List<Employee> employees = new List<Employee>();
            employees.Add(emp);
            employees.Add(manager);
            employees.Add(wait);

            decimal totalPay = 0;
            foreach(Employee employee in employees)
            {
                totalPay += employee.CalculatePay();
            }

            total.Text = totalPay.ToString();
        }
    }
}
