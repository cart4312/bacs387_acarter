﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Employee
    {
        public double Hours { get; set; }
        public decimal PayRate { get; set; }
        public virtual decimal CalculatePay()
        {
            return (decimal)Hours * PayRate;
        }
    }
}
