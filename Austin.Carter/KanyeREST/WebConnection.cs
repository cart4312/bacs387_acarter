﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace KanyeREST
{
    class WebConnection
    {
        private static string domain = "http://www.kanyerest.xyz/api/track/";

        public string download(Song s)
        {
            string url = domain + s.song;
            var client = new WebClient();
            string text = "";

            try
            {
                text = client.DownloadString(url);
            }
            catch(Exception)
            {
                text = "Error";
            }

            return text;
        }
    }
}
