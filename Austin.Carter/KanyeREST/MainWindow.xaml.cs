﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;

namespace KanyeREST
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void convertbutton_Click(object sender, RoutedEventArgs e)
        {
            WebConnection web = new WebConnection();
            Song s = new Song();
            s.song = inputBox.Text;
            s.song = s.fixSong();
            JsonInput inputObject = new JsonInput();
            inputObject.Json = web.download(s);

            /*
             * MS: good way of handling this.  You could also 
             * throw an exception and then handle it here.
             * 
             * Some people consider the way you did it more technically
             * correct since it's not really an exception, it's just an error.
             * 
             * Others feel like it's more natural to handle all edge cases as 
             * exceptions.  Every shop has their own way of doing this so 
             * check around when you land a job and see how they do things in house. 
             */
            if( inputObject.Json == "Error"){
                inputBox.Text = "";
                MessageBox.Show("Please enter in a valid song");
            }
            else
            {
                Converter converterObject = new Converter();
                Track convertedTrack = converterObject.Convert(inputObject);
                outputBox.Text = convertedTrack.Lyrics;
            }
            
        }
    }
}
