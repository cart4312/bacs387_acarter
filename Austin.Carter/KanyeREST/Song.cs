﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KanyeREST
{
    class Song
    {
        /*
         * MS: Watch capitalization in .NET
         * See the conventions document on Canvas
         */
        public string song { get; set; }

        public string fixSong()
        {
            song = song.Replace(" ", "_");
            song = song.ToLower();
            return song;
        }
    }
}
