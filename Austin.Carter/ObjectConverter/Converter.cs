﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectConverter
{
    class Converter
    {
        public Track Convert(JsonInput input)
        {

            Track theTrack = JsonConvert.DeserializeObject<Track>(input.Json);

            return theTrack;
        }
    }
}
