﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectConverter
{
    class Track
    {
        public string Album { get; set; }
        public string Title { get; set; }
        public string Lyrics { get; set; }

    }
}
