﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encapsulation
{
    class Person
    {
        //field (down below without the slashes)
        //public string name;
        //property
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }

        public int CalculateAge(DateTime today)
        {
            TimeSpan age = today - BirthDate;
            return age.Days / 365;
        }

    }
}
